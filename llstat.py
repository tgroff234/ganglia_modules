#!/usr/bin/python

import os
import sys
import subprocess
import re

descriptors = list()

def metric_handler(name):
	metric = 0
	if 'mds' in hostname:
		out = subprocess.Popen(['/usr/bin/llstat', 'mdt'], stdout=subprocess.PIPE).communicate()[0]
	elif 'oss' in hostname:
		out = subprocess.Popen(['/usr/bin/llstat', 'ost'], stdout=subprocess.PIPE).communicate()[0]
	else:
		print 'Error: must be running on lustre controller'

	if(len(out)<300):
		print 'Error: lustre is not running'
		
	line = [x for x in out.split('\n') if name in x][0]
	
	match = re.search('[\d\.]+$', line)	
	if match:
		met = match.group(0)

	if name == 'snapshot_time':
		metric = float(met)
	else:
		metric = int(met)

	return metric

def metric_init(params):
	global descriptors
	global hostname
	hostname = os.environ["HOSTNAME"]

	d = {
		"call_back" : metric_handler,
		"time_max" : 60,
		"value_type" : "uint'",
		"units" : "",
		"slope" : "both",
		"format" : "%u",
		"description" : "llstat metrics"
	}
	
	if 'mds' in hostname:
		names = [
			'snapshot_time',
			'req_waittime',
			'req_qdepth',
			'req_active',
			'req_timeout',
			'reqbuf_avail',
			'mds_connect',
			'obd_ping' ]
	elif 'oss' in hostname:
		names = [
			'snapshot_time',
			'req_waittime',
			'req_qdepth',
			'req_active',
			'req_timeout',
			'reqbuf_avail',
			'ost_create',
			'ost_get_info',
			'ost_connect' ]
	else:
		print 'error'
		sys.exit(-1)

	for name in names:
		dtemp = {'name' : name}
		dtemp.update(d)
		if name == 'snapshot_time': dtemp['value_type'] = 'float'
		descriptors.append(dtemp)
	
	return descriptors


def metric_cleanup():
	pass

if __name__ == "__main__":
	descriptors = metric_init(None)
	for d in descriptors:
		print '%s: %s'%(d['name'], str(d['call_back'](d['name'])))

