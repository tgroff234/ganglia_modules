#!/usr/bin/python

import os
import sys
import subprocess
import re


descriptors = list()


def get_counter(name):
	metric = 0

	for ca in os.listdir("/sys/class/infiniband"):
		for port in os.listdir('/sys/class/infiniband/%s/ports'%ca):
			out = subprocess.Popen(['/usr/sbin/perfquery', '-C', ca, '-P', port], stdout=subprocess.PIPE).communicate()[0]
			try: line = [x for x in out.split('\n') if name in x][0]
			except IndexError: print name
			if line:
				match = re.search('\d+$', line)
				if match:
					metric += int(match.group(0))
	return metric


def metric_init(params):

	global descriptors
	global syspath
	syspath = "/sys/class/infiniband/mlx4_0/ports/1/counters/"
	
	counters = { 
		'VL15Dropped' : 0,
		'ExcessiveBufferOverrunErrors' : 0,
		'LinkDownedCounter' : 0,
		'LinkErrorRecoveryCounter' : 0,
		'LocalLinkIntegrityErrors' : 0,
		'PortRcvConstraintErrors' : 0,
		'PortRcvData' : 0,
		'PortRcvErrors' : 0,
		'PortRcvPkts' : 0,
		'PortRcvRemotePhysicalErrors' : 0,
		'PortRcvSwitchRelayErrors' : 0,
		'PortXmitConstraintErrors' : 0,
		'PortXmitData' : 0,
		'PortXmitDiscards' : 0,
		'PortXmitPkts' : 0,
		'SymbolErrorCounter' : 0	
	}

	d = {
		'call_back' : get_counter,
		'time_max' : 30,
		'value_type' : 'uint',
		'units' : '',
		'slope' : 'none',
		'format' : '%u',
		'description' : "Infiniband counter metric"
	}

	for metric in counters:
		d1 = { 'name' : metric }
		d1.update(d)

		descriptors.append(d1)

	return descriptors

def metric_cleanup():
	pass

if __name__ == "__main__":
	descriptors = metric_init(None)
	for descriptor in descriptors:
		print("%s: %d"%(descriptor['name'], descriptor['call_back'](descriptor['name'])))
		
