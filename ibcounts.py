#!/usr/bin/python

import subprocess
import subprocess


descriptors = list()

def metric_handler(name):
	errors_raw = subprocess.Popen(['ibqueryerrors'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0]
	errors = errors_raw.split('\n')
	
	for line in errors:
		if re.search('GUID', line, re.MULTILINE) is None:
			errors.remove(line)


	if name == 'PortXmitWait':
		

def metric_init(params):
	global descriptors
	d = {
		'callback' : metric_handler,
		'time_max' : 60,
		'value_type' : 'uint',
		'units' : '',
		'slope' : 'both',
		'format' : '%u',
		'description': "infiniband fabric error counters",
		'groups' : 'network'
	    }
	
	for metric in ('PortXmitWait', 'SymbolErrorCounter', 'PortXmitDiscards', 'PortRcvErrors', 'PortRcvSwitchRelayErrors','LinkDownedCounter'):
		d1 = { 'name' : metric }
		d1.update(d)
		descriptors.append(d1)

	return descriptors



def metric_cleanup():
	pass
