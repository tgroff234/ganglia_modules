#!/usr/bin/python

import os
import subprocess
import re

descriptors = list()

def metric_handler(name):
	out = subprocess.Popen(["/opt/bin/monitor_ib_health.sh"], stdout=subprocess.PIPE).communicate()[0]
	status  = re.sub("(mlx\d_\d-\d:)|( ;; \n)", "", out)
	return status

def metric_init(params):
	global descriptors

	descriptors.append({
				'name' : 'ib_status',
				'call_back' : metric_handler,
				'time_max' : 60,
				'value_type' : 'string',
				'units' : '',
				'slope' : 'zero',
				'format' :  '%s',
				'description' : 'status of IB fabric health',
				'groups' : 'network'
			})
	return descriptors

def metric_cleanup():
	pass

if __name__ == '__main__':
	d = metric_init(None)[0]
	print d['call_back'](d['name'])
